TARDIS:AddControl({
	id = "wooden_scanner_window",
	tip_text = "Scanner",
	serveronly=true,
	power_independent = false,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		local scannerwindow = self:GetPart("wooden_window")
		if not IsValid(scannerwindow) then return end
		scannerwindow:Toggle( !scannerwindow:GetOn(), ply )
	end,
})

TARDIS:AddControl({
	id = "wooden_change_lights",
	tip_text = "Light Color Panel",
	serveronly=true,
	power_independent = false,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		local no = self:GetData("wooden_light_no", self.metadata.Interior.wooden_default_light_no or 0)
		no = (no + 1) % table.Count(self.metadata.Interior.Light.states)
		self:SetData("wooden_light_no", no)
		self:ApplyLightState("" .. no)
	end,
})

