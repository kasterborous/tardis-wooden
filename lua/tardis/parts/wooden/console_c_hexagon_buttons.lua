local PART={}
PART.ID = "wooden_c_hexagon_buttons"
PART.Name = "Wooden Hexagon Buttons"
PART.Model = "models/emerald/uriel/tardis/hexagon_buttons.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "lime/uriel/public/tardis/button.wav"

TARDIS:AddPart(PART)