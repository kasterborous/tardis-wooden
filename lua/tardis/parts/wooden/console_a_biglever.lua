local PART={}
PART.ID = "wooden_a_biglever"
PART.Name = "Wooden Cloak"
PART.Model = "models/emerald/uriel/tardis/faggot.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.0
PART.Sound = "lime/uriel/public/tardis/timelever.wav"

TARDIS:AddPart(PART)