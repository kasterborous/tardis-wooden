local PART={}
PART.ID = "wooden_toprot"
PART.Name = "Wooden Top Rotor"
PART.Model = "models/emerald/uriel/tardis/toprot.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "lime/uriel/public/tardis/button.wav"

TARDIS:AddPart(PART)