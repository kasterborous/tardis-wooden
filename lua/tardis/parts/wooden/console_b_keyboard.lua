local PART={}
PART.ID = "wooden_b_keyboard"
PART.Name = "Wooden Keyboard"
PART.Model = "models/emerald/uriel/tardis/keyboard.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "lime/uriel/public/tardis/keyboard.wav"

TARDIS:AddPart(PART)