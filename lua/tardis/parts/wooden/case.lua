local PART={}
PART.ID = "wooden_case"
PART.Name = "Wooden Glass Case"
PART.Model = "models/emerald/uriel/tardis/glass_case.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)