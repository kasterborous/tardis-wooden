local PART={}
PART.ID = "wooden_e_wide_buttons"
PART.Name = "Uriel Emerald Wide Buttons"
PART.Model = "models/emerald/uriel/tardis/float_button.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "lime/uriel/public/tardis/button.wav"

TARDIS:AddPart(PART)