local PART={}
PART.ID = "wooden_c_screen_button"
PART.Name = "Wooden Screen Button"
PART.Model = "models/emerald/uriel/tardis/cloister_button.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "lime/uriel/public/tardis/button.wav"

TARDIS:AddPart(PART)