local PART={}
PART.ID = "wooden_f_roundbtns"
PART.Name = "Wooden Round Buttons"
PART.Model = "models/emerald/uriel/tardis/door_buttons.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "lime/uriel/public/tardis/button.wav"

TARDIS:AddPart(PART)