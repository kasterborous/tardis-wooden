local PART={}
PART.ID = "wooden_details"
PART.Name = "Wooden Details"
PART.Model = "models/emerald/uriel/tardis/details.mdl"
PART.AutoSetup = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "wooden_f_hitbox"
PART.Name = "Wooden TARDIS hitbox"
PART.Model = "models/hunter/plates/plate025x05.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = false
PART.Sound = "lime/uriel/public/tardis/button.wav"

function PART:Initialize()
	self:SetColor(Color(255,255,255,0))
	self:SetCollisionGroup(COLLISION_GROUP_WORLD)
end

TARDIS:AddPart(PART)

PART.ID = "wooden_b_hitbox"
TARDIS:AddPart(PART)

PART.Model = "models/hunter/plates/plate025x025.mdl"
PART.ID = "wooden_d_hitbox1"
TARDIS:AddPart(PART)
PART.ID = "wooden_d_hitbox2"
TARDIS:AddPart(PART)

PART.Model = "models/hunter/blocks/cube025x2x025.mdl"
PART.ID = "wooden_d_hitbox_long"
TARDIS:AddPart(PART)

PART.Model = "models/hunter/plates/plate.mdl"
PART.ID = "wooden_f_hitbox_small"
TARDIS:AddPart(PART)

PART.Model = "models/hunter/blocks/cube025x05x025.mdl"
PART.ID = "wooden_e_hitbox"
TARDIS:AddPart(PART)

PART.ID = "wooden_c_hitbox"
TARDIS:AddPart(PART)
