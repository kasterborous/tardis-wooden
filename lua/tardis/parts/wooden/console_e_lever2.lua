local PART={}
PART.ID = "wooden_e_lever2"
PART.Name = "Wooden Lever 2"
PART.Model = "models/emerald/uriel/tardis/cunt2here.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5
PART.Sound = "lime/uriel/public/tardis/leveron.wav"

TARDIS:AddPart(PART)