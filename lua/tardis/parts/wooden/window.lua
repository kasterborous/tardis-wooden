local PART={}
PART.ID = "wooden_window"
PART.Name = "Wooden Scanner Window"
PART.Model = "models/emerald/uriel/tardis/scanner.mdl"
PART.AutoSetup = true
PART.Collision = false
PART.Animate = true
PART.AnimateSpeed = 0.34
PART.Sound = "lime/uriel/public/tardis/window.wav"

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end

	function PART:Toggle( bEnable, ply )
		self:SetOn(bEnable)
		self:EmitSound(Sound(self.Sound))
		self:SetCollide(not bEnable)
	end
end

TARDIS:AddPart(PART)