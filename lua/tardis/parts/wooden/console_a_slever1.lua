local PART={}
PART.ID = "wooden_a_slever1"
PART.Name = "Wooden Small Lever 1"
PART.Model = "models/emerald/uriel/tardis/small_lever_1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5
PART.Sound = "lime/uriel/public/tardis/small_lever.wav"

TARDIS:AddPart(PART)