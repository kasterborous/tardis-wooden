local PART={}
PART.ID = "wooden_e_keypad"
PART.Name = "Wooden Keypad"
PART.Model = "models/emerald/uriel/tardis/keypad.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "lime/uriel/public/tardis/keypad.wav"

TARDIS:AddPart(PART)