local PART={}
PART.ID = "wooden_c_small_monitor"
PART.Name = "Wooden Small Monitor"
PART.Model = "models/emerald/uriel/tardis/monitor_viewer.mdl"
PART.AutoSetup = true
PART.Collision = true


if SERVER then
	function PART:Use(ply)
		if not self.interior:GetScreensOn() then
			TARDIS:Control(self.Control, ply)
		end
	end
end
TARDIS:AddPart(PART)