local PART={}
PART.ID = "wooden_f_scannerbtn"
PART.Name = "Wooden Scanner Button"
PART.Model = "models/emerald/uriel/tardis/scanner_button.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "lime/uriel/public/tardis/button.wav"

TARDIS:AddPart(PART)