local PART={}
PART.ID = "wooden_a_phone"
PART.Name = "Wooden TARDIS Phone"
PART.Model = "models/emerald/uriel/tardis/phone.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "lime/uriel/public/tardis/phone.wav"

if SERVER then
	function PART:Use(ply)
		self.interior:Timer("", 1.1, function()
			TARDIS:Control(self.Control, ply)
		end)
	end
end

TARDIS:AddPart(PART)