local PART={}
PART.ID = "wooden_d_biglever"
PART.Name = "Wooden Throttle"
PART.Model = "models/emerald/uriel/tardis/stupid.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.0
PART.Sound = "lime/uriel/public/tardis/timelever.wav"

TARDIS:AddPart(PART)