local PART={}
PART.ID = "wooden_rot"
PART.Name = "Wooden Rotor"
PART.Model = "models/emerald/uriel/tardis/rot_uriel.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1

if CLIENT then
	function PART:Initialize()
		self.timerotor={}
		self.timerotor.pos=0
		self.timerotor.mode=1
	end

	function PART:Think()
		local exterior=self.exterior

		local flight = exterior:GetData("flight")
		local teleport = exterior:GetData("teleport")
		local vortex = exterior:GetData("vortex")
		local active = flight or teleport or vortex

		if self.timerotor.pos > 0 or active then
			if self.timerotor.pos==0 then
				self.timerotor.mode=1
			elseif self.timerotor.pos==1 and active then
				self.timerotor.pos=0
			end

			self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.3 )
			self:SetPoseParameter( "rotor", self.timerotor.pos )
		end
	end
end

TARDIS:AddPart(PART)