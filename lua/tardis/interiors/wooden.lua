-- The TARDIS of Artonymaaa

local T = {}
T.Base = "base"
T.Name = "Wooden TARDIS"
T.ID = "wooden"

T.Versions = {
	allow_custom = true,
	randomize_custom = true,
}

local WARDROBE_FILENAME = "models/molda/nick/nick_tardis_exterior10.mdl"
if file.Exists(WARDROBE_FILENAME, "GAME") then
	T.Versions.main = {
		id = "wooden_wardrobe",
	}
	T.Versions.other = {
		{
			name = "TT Capsule",
			id = "wooden",
		},
	}
end

local LightStateColors = {
	["0"] = { color = Color(30, 10, 0.5), },
	["1"] = { color = Color(100, 40, 2), },
	["2"] = { color = Color(255,100,5), },
	["3"] = { color = Color(255,255,150), },
	["4"] = { color = Color(135, 0, 255), },
	["5"] = { color = Color(10,0,255), },
	["6"] = { color = Color(10,0,255), },
	["7"] = { color = Color(10,0,255), },
	["8"] = { color = Color(10,0,255), },
	["9"] = { color = Color(40, 170, 220), },
	["10"] = { color = Color(80, 255, 5), },
	["11"] = { color = Color(10, 140, 5), },
}

local LampStateColors = {
	["0"] = { color = Color(255, 85, 0), brightness = 0.5, },
	["1"] = { color = Color(255, 85, 0), brightness = 1.0, },
	["2"] = { color = Color(255, 85, 0), },
	["3"] = { color = Color(255,255,150), },
	["4"] = { color = Color(135, 0, 255), },
	["5"] = { color = Color(255, 85, 0), },
	["6"] = { color = Color(135, 0, 255), },
	["7"] = { color = Color(10,0,255), },
	["8"] = { enabled = false, },
	["9"] = { color = Color(40, 170, 220), },
	["10"] = { color = Color(80, 255, 5), },
	["11"] = { color = Color(10, 140, 5), },
}

T.Interior = {
	Model = "models/emerald/uriel/tardis/interior.mdl",
	Portal = {
		pos = Vector(-0.6776,-291.244,178.054),
		ang = Angle(0,90,0),
		width = 50,
		height = 98
	},
	Fallback = {
		pos = Vector(-0.6776,-258,139),
		ang = Angle(0,90,0),
	},
	ExitDistance = 1600,
	Screens = {
		{
			pos = Vector(15.817, 26.021, 77.844),
			ang = Angle(0, 120, 80),
			width = 175,
			height = 92,
			visgui_rows = 2
		}
	},
	ScreensEnabled = false,
	Sounds = {
		Teleport = {
			demat = "lime/uriel/public/tardis/demat.wav",
			mat = "lime/uriel/public/tardis/mat.wav"
		},
		FlightLoop = "lime/uriel/public/tardis/flightoutside.wav",
		Cloister = "lime/uriel/public/tardis/cloister_loop.wav",
	},
	Parts = {
		door = {
			model = "models/artixc/exteriors/mk3_door.mdl",
			posoffset=Vector(0,-15,-43.55),
			--posoffset = Vector(0,0,-0.85),
			angoffset = Angle(0,180,0),
			matrixScale = Vector(1.0, 1.22, 1.01),
		},

		wooden_console = {},

		wooden_a_phone = { pos = Vector(0,0,.5), },
		wooden_a_biglever = { pos = Vector(0,0,.5), },
		wooden_a_slever1 = { pos = Vector(0,0,.5), },
		wooden_a_slever2 = { pos = Vector(0,0,.5), },
		wooden_a_slever3 = { pos = Vector(0,0,.5), },
		wooden_a_slever4 = { pos = Vector(0,0,.5), },

		wooden_b_keyboard = { pos = Vector(0,0,.5), },
		wooden_b_hitbox = { pos = Vector(14.811, 13.444, 71.29), ang = Angle(22.221, -29.264, 1.166), scale = 0.55, },

		wooden_c_cirquit = { pos = Vector(0,0,.5), },
		wooden_c_screen_button = { pos = Vector(0,0,.5), },
		wooden_c_hexagon_buttons = { pos = Vector(0,0,.5), },
		wooden_c_small_monitor = { pos = Vector(0,0,.5), },
		wooden_c_hitbox = { pos = Vector(15.689, 38.977, 69.236), ang = Angle(-19.738, 30.761, -0.664), scale = 0.3, },

		wooden_d_biglever = { pos = Vector(0,0,.5), },
		wooden_d_hitbox1 = { pos = Vector(-7.521, 44.753, 68.748), ang = Angle(1.643, 8.782, -18.734), scale = 0.45, },
		wooden_d_hitbox2 = { pos = Vector(5.673, 44.426, 68.848), ang = Angle(-23.631, -101.254, -174.062), scale = 0.45, },
		wooden_d_hitbox_long = { pos = Vector(-0.444, 51.359, 65.172), ang = Angle(0, 89.65, 0), scale = 0.23, },

		wooden_e_wide_buttons = { pos = Vector(0,0,.5), },
		wooden_e_keypad = { pos = Vector(0,0,.5), },
		wooden_e_lever1 = { pos = Vector(0,0,.5), },
		wooden_e_lever2 = { pos = Vector(0,0,.5), },
		wooden_e_hitbox = { pos = Vector(-27.426, 27.689, 66.273), ang = Angle(-15.536, -28.018, 2.167), scale = 0.3, },

		wooden_f_scannerbtn = { pos = Vector(0,0,.5), },
		wooden_f_roundbtns = { pos = Vector(0,0,.5), },
		wooden_f_hitbox = { pos = Vector(-17.291, 13.836, 70.984), ang = Angle(-23.971, 31.182, 1.46), scale = 0.45, },
		wooden_f_hitbox_small = { pos = Vector(-16.678, 5.44, 67.434), ang = Angle(0, 13.64, 0), scale = 1.5, },

		wooden_details = { pos = Vector(0,0,.5), },
		wooden_details_2 = { pos = Vector(0,0,.5), },
		wooden_details_3 = { pos = Vector(0,0,.5), },
		wooden_details_4 = { pos = Vector(0,0,.5), },

		wooden_window = {},
		wooden_toprot = {},

		wooden_bigrdls = {},
		wooden_case = {},
		wooden_rot = {},
	},
	TipSettings = {
		view_range_min = 40,
		view_range_max = 60,
	},
	PartTips = {
		wooden_a_phone = { pos = Vector(-0.559, 4.96, 71.175), text = "Phone", },
		wooden_a_biglever = { pos = Vector(7.179, -2.129, 69.175), right = true, },
		wooden_a_slever1 = { pos = Vector(-2.673, -4.676, 68.516), right = true, down = true, },
		wooden_a_slever2 = { pos = Vector(-4.886, -3.967, 68.355), right = true, },
		wooden_a_slever3 = { pos = Vector(-7.214, -3.551, 68.281), },
		wooden_a_slever4 = { pos = Vector(-9.357, -3.479, 68.282), down = true, },

		wooden_b_keyboard = { pos = Vector(23.388, 13.352, 66.131), },
		wooden_b_hitbox = { pos = Vector(14.811, 13.444, 71.29), },

		wooden_c_cirquit = { pos = Vector(22.502, 29.274, 67.167), down = true, },
		wooden_c_screen_button = { pos = Vector(22.827, 36.529, 66.345), down = true, right = true, },
		wooden_c_hexagon_buttons = { pos = Vector(19.347, 27.883, 70.363), },
		wooden_c_hitbox = { pos = Vector(15.689, 38.977, 69.236), right = true, },
		wooden_c_small_monitor = { pos = Vector(12.281, 31.84, 77.767), right = true, },

		wooden_d_biglever = { pos = Vector(-3.387, 44.826, 69.539), },
		wooden_d_hitbox1 = { pos = Vector(-7.521, 44.753, 68.748), down = true, right = true, },
		wooden_d_hitbox2 = { pos = Vector(5.673, 44.426, 68.848), down = true, },
		wooden_d_hitbox_long = { pos = Vector(5.983, 50.712, 66.189), down = true, right = true, },

		wooden_e_wide_buttons = { pos = Vector(-19.947, 44.325, 66.205), down = true, },
		wooden_e_keypad = { pos = Vector(-22.823, 35.643, 66.428), right = true, down = true, },
		wooden_e_lever1 = { pos = Vector(-17.192, 39.351, 70.549), },
		wooden_e_lever2 = { pos = Vector(-21.931, 28.71, 70.323), },
		wooden_e_hitbox = { pos = Vector(-27.426, 27.689, 66.273), },

		wooden_f_scannerbtn = { pos = Vector(-23.974, 7.914, 66.162), },
		wooden_f_roundbtns = { pos = Vector(-24.49, 17.771, 68.677), },
		wooden_f_hitbox = { pos = Vector(-17.291, 13.836, 70.984), },
		wooden_f_hitbox_small = { pos = Vector(-16.678, 5.44, 67.434), },
	},
	Controls = {
		--wooden_console = "thirdperson_careful",

		wooden_a_phone = "virtualconsole",
		wooden_a_biglever = "teleport",
		wooden_a_slever1 = "vortex_flight",
		wooden_a_slever2 = "cloak",
		wooden_a_slever3 = "doorlock",
		wooden_a_slever4 = "handbrake",

		wooden_b_keyboard = "coordinates",
		wooden_b_hitbox = "thirdperson",

		wooden_c_cirquit = "destination",
		wooden_c_screen_button = "toggle_screens",
		wooden_c_hexagon_buttons = "isomorphic",
		wooden_c_hitbox = "hads",
		wooden_c_small_monitor = "scanner",

		wooden_d_biglever = "power",
		wooden_d_hitbox1 = "repair",
		wooden_d_hitbox2 = "redecorate",
		wooden_d_hitbox_long = "wooden_change_lights",

		wooden_e_wide_buttons = "physlock",
		wooden_e_keypad = "virtualconsole",
		wooden_e_lever1 = "flight",
		wooden_e_lever2 = "float",
		wooden_e_hitbox = "door",

		wooden_f_scannerbtn = "wooden_scanner_window",
		wooden_f_roundbtns = "fastreturn",
		wooden_f_hitbox = "thirdperson",
		wooden_f_hitbox_small = "engine_release",
	},
	IdleSound = {
		{
			path = "lime/uriel/public/tardis/interior_art_o_nym.wav",
			volume = 10
		}
	},
	LightOverride = {
		basebrightness = 0.05,
		nopowerbrightness = 0.015
	},
	Light = {
		color = Color(255,100,0),
		warncolor = Color(176, 0, 0),
		pos = Vector(0,24.5,150),
		brightness = 6.5,

		states = table.Copy(LightStateColors),
	},
	wooden_default_light_no = 2,
	Lights = {
		{
			color = Color(3,0,76.5),
			pos = Vector(0, 0, 40),
			brightness = 0.2,
			nopower = true,
			warncolor = Color(60, 0, 0),
		},
	},
	Lamps = {
		{
			color = Color(255, 85, 0),
			texture = "effects/flashlight/soft",
			fov = 90,
			distance = 1024,
			brightness = 3,
			pos = Vector(27.86328125, 40.248046875, 169.451171875),
			ang = Angle(-5.4768762588501, 33.863677978516, 1.9198234081268),
			states = table.Copy(LampStateColors),
		},
		{
			color = Color(255, 85, 0),
			texture = "effects/flashlight/soft",
			fov = 90,
			distance = 1024,
			brightness = 3,
			pos = Vector(-0.513671875, 47.5869140625, 169.9794921875),
			ang = Angle(5.1564240455627, 85.91919708252, -34.709289550781),
			states = table.Copy(LampStateColors),
		},
		{
			color = Color(255, 85, 0),
			texture = "effects/flashlight/soft",
			fov = 90,
			distance = 1024,
			brightness = 3,
			pos = Vector(-31.5126953125, 35.50390625, 172.9599609375),
			ang = Angle(16.616622924805, 157.48104858398, -2.8157348632813),
			states = table.Copy(LampStateColors),
		},
		{
			color = Color(255, 85, 0),
			texture = "effects/flashlight/soft",
			fov = 90,
			distance = 1024,
			brightness = 3,
			pos = Vector(45.03125, -250.1220703125, 246.8662109375),
			ang = Angle(31.259328842163, -13.525435447693, -164.45452880859),
			states = table.Copy(LampStateColors),
		},
		{
			color = Color(255, 85, 0),
			texture = "effects/flashlight/soft",
			fov = 90,
			distance = 1024,
			brightness = 3,
			pos = Vector(-53.8671875, -258.6787109375, 144.453125),
			ang = Angle(-39.004199981689, -176.82760620117, -112.97276306152),
			states = table.Copy(LampStateColors),
		},
		{
			color = Color(255, 85, 0),
			texture = "effects/flashlight/soft",
			fov = 161.81817626953,
			distance = 1024,
			brightness = 0.95454543828964,
			pos = Vector(-234.61474609375, -113.9970703125, 263.7265625),
			ang = Angle(54.966579437256, -142.69497680664, 32.9592628479),
			states = table.Copy(LampStateColors),
		},
		{
			color = Color(255, 85, 0),
			texture = "effects/flashlight/soft",
			fov = 60,
			distance = 1608.3636474609,
			brightness = 3.6818182468414,
			pos = Vector(-2.230712890625, -250.1171875, 267.7197265625),
			ang = Angle(86.403907775879, -81.795417785645, -129.67698669434),

			states = table.Copy(LampStateColors),

			nopower = true,

			off = {
				fov = 90,
				brightness = 0.1,
			},
		},

	},
	TextureSets = {
		on = {
			prefix = "models/emerald/uriel/tardis/",
			{ "self", 2, "floor_2"},
			{ "self", 0, "round_things"},
			{ "wooden_bigrdls", 0, "floor_2"},
			{ "wooden_bigrdls", 1, "round_things"},
		},
		off = {
			prefix = "models/emerald/uriel/tardis/",
			{ "self", 2, "floor_2_off"},
			{ "self", 0, "round_things_off"},
			{ "wooden_bigrdls", 0, "floor_2_off"},
			{ "wooden_bigrdls", 1, "round_things_off"},
		},
	},
	CustomHooks = {
		power = {
			"PowerToggled",
			function(self)
				local power = self:GetData("power-state")
				if power then
					self:ApplyTextureSet("on")
				else
					self:ApplyTextureSet("off")
				end
			end,
		},
	},
}

T.Exterior = {
	Model="models/artixc/exteriors/mk3.mdl",
	Mass = 2000,
	Portal={
		pos=Vector(18.85, 0, 52.6),
		ang=Angle(0,0,0),
		width=26,
		height=87,
		thickness = 25,
		inverted = true,
	},
	Fallback={
		pos=Vector(44,0,7),
		ang=Angle(0,0,0)
	},
	Light = {
		enabled = false,
	},
	Parts = {
		door = {
			model = "models/artixc/exteriors/mk3_door.mdl",
			posoffset=Vector(-5,12.54,-43.55),
			angoffset=Angle(0,0,0),
		},
		vortex = {
			model="models/artonym/custom/tardis/uriel_vortex.mdl",
			pos=Vector(0,0,100),
			ang=Angle(0,90,0),
			scale=10
		},
	},
	Sounds = {
		Teleport = {
			demat = "lime/uriel/public/tardis/demat.wav",
			mat = "lime/uriel/public/tardis/mat.wav"
		},
		FlightLoop = "lime/uriel/public/tardis/flightoutside.wav",
		Cloak = "lime/uriel/public/tardis/verse_cloak.wav",
		CloakOff = "lime/uriel/public/tardis/reverse_cloak.wav",
		Door={
			enabled=true,
			open="vtalanov98/hellbentext/doorext_open.wav",
			close="vtalanov98/hellbentext/doorext_close.wav",
		},
	},
}

TARDIS:AddInterior(T)
