
local WARDROBE_FILENAME = "models/molda/nick/nick_tardis_exterior10.mdl"
local VORTEX_MAT_FILENAME = "models/molda/tuat/vortex/vortex"
local VORTEX_MOD_FILENAME = "models/molda/tuat/vortex/tuatvortex.mdl"


if not file.Exists(WARDROBE_FILENAME, "GAME") then
	return
end

if not file.Exists("materials/" .. VORTEX_MAT_FILENAME .. ".vmt", "GAME") then
	VORTEX_MAT_FILENAME = "models/molda/custom/vortexes/vortex_2/vortex"
end

if not file.Exists(VORTEX_MOD_FILENAME, "GAME") then
	VORTEX_MOD_FILENAME = "models/molda/custom/vortexes/2_vortex.mdl"
end

local T = {
	Base = "wooden",
	Name = "Wooden TARDIS (Wardrobe)",
	ID = "wooden_wardrobe",
	IsVersionOf = "wooden",

	Interior = {
		ScreensEnabled = true,
		Parts={
			door={
				model="models/molda/nick/nick_tardis_door1.mdl",
				posoffset=Vector(5, 15,-47.5),
				angoffset=Angle(0,180,0),
				matrixScale = Vector(1.0, 1.3, 1.01),
			},
		},
		TextureSets = {
			vortexes = {
				{ "self", 3, VORTEX_MAT_FILENAME, },
				{ "self", 5, VORTEX_MAT_FILENAME, },
				{ "wooden_c_small_monitor", 1, VORTEX_MAT_FILENAME, },
			},
		},
		CustomHooks = {
			initial = {
				"Initialize",
				function(self)
					self:ApplyTextureSet("vortexes")
				end,
			},
		},
	},
	Exterior = {
		Model=WARDROBE_FILENAME,
		ScannerOffset = Vector(25,0,50),
		ExcludedSkins = { 3, 4 },
		Portal = {
			pos=Vector(14,8,47.5),
			ang=Angle(0,0,0),
			width=30.09568,
			height=95
		},
		Sounds = {
			Lock = "lime/uriel/public/tardis/lock_art.wav",
			Door = {
				enabled = true,
				open = "lime/uriel/public/tardis/door_1.wav",
				close = "lime/uriel/public/tardis/door_2.wav"
			},
			Lock="atrovis/nick/tardis/nick_lock.wav",
			Door = {
				enabled=true,
				open="molda/nick/nick_dooropen.wav",
				close="molda/nick/nick_door_close_.wav"
			},
		},
		Fallback = {
			pos=Vector(33,10,20),
			ang=Angle(0,0,0)
		},
		Light = {
			enabled=false,
		},
		Parts={
			door = {
				model="models/molda/nick/nick_tardis_door1.mdl",
				posoffset=Vector(-14, -8, -47.5),
				angoffset=Angle(0,0,0)
			},
			vortex = {
				model=VORTEX_MOD_FILENAME,
				pos=Vector(0,0,0),
				ang=Angle(0,180,0),
				scale=6
			}
		},
	},
}

TARDIS:AddInterior(T)
