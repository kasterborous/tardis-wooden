SonicSD:AddSonic({
	ID="urielsonic",
	Name="Uriel's Sonic Screwdriver",
	ViewModel="models/emerald/uriel/tardis/1stpersonsonic.mdl",
	WorldModel="models/emerald/uriel/tardis/3rdpersonsonic.mdl",
	LightPos=Vector(20,-3,5.1),
	SoundLoop = "lime/uriel/public/tardis/sonix.wav"
})